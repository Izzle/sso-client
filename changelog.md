0.0.6
-----
- Fixed get method call

0.0.5
-----
- Added client cache manager lifetime setter
- Added error handling

0.0.4
-----
- Added Cache Manager
- Changed method parameter (create, update) to User

0.0.3
-----
- Added new User class
- Changed method response to User

0.0.2
-----
- Changed LoginClient class to Client
- Changed class namespace
- Changed some properties and methods to psr-2 standard

0.0.1
-----
- First beta