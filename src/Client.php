<?php
namespace Izzle\SSO\Client;

use GuzzleHttp\Client as GuzzleClient;
use Izzle\SSO\Client\Entities\User;
use Izzle\SSO\Client\Exception\JsonException;
use Psr\Http\Message\ResponseInterface;

final class Client
{
    /**
     * @var \GuzzleHttp\Client $http
     */
    private static $http;

    /**
     * @var string $api_token
     */
    private static $api_token = '';
    
    /**
     * @var int
     */
    private static $lifetime = 86400; // Seconds
    
    /**
     * @var string
     */
    private static $base_url = '';

    private function __construct() {}
    private function __clone() {}

    /**
     * @throws \Exception
     */
    private static function initialize()
    {
        if (empty(self::$api_token)) {
            throw new \Exception('Api token cannot be empty');
        }
        
        if (empty(self::$base_url)) {
            throw new \Exception('Api base url cannot be empty');
        }

        if (is_null(self::$http)) {
            self::$http = new GuzzleClient([
                'base_uri' => self::$base_url,
                'headers' => [
                    'x-auth' => self::$api_token
                ]
            ]);
        }
        
        CacheManager::setLifetime(self::$lifetime);
    }
    
    /**
     * @param string $url
     */
    public static function setBaseUrl($url)
    {
        if (!is_string($url)) {
            throw new \InvalidArgumentException('Param url must be from type string');
        }
        
        self::$base_url = $url;
    }
    
    /**
     * @param int $lifetime
     */
    public static function setCacheLifetime($lifetime)
    {
        self::$lifetime = (int) $lifetime;
        CacheManager::setLifetime(self::$lifetime);
    }

    /**
     * @param string $email
     * @param string $password
     * @throws \Exception
     * @return bool
     */
    public static function check($email, $password)
    {
        self::initialize();
    
        try {
            $response = self::$http->post('logins', [
                'form_params' => [
                    'email' => $email,
                    'password' => $password
                ]
            ]);
    
            return $response->getStatusCode() == 200;
        } catch (\Exception $e) { }
        
        return false;
    }
    
    /**
     * @param $user_id
     * @throws \Exception
     * @return User|null
     */
    public static function show($user_id)
    {
        $user = self::getCache(null, $user_id);
        if (!is_null($user)) {
            return $user;
        }
        
        self::initialize();
        
        try {
            $response = self::$http->get('users/' . $user_id);
    
            $user = self::manageResponse($response);
            if (!is_null($user)) {
                self::setCache($user, $user->getEmail(), $user_id);
            }
    
            return $user;
        } catch (\Exception $e) { }
        
        return null;
    }
    
    /**
     * @param string $email
     * @throws \Exception
     * @return User|null
     */
    public static function get($email)
    {
        $user = self::getCache($email);
        if (!is_null($user)) {
            return $user;
        }
        
        self::initialize();
    
        try {
            $response = self::$http->post('users/show', [
                'form_params' => [
                    'email' => $email
                ]
            ]);
    
            $user = self::manageResponse($response);
            if (!is_null($user)) {
                self::setCache($user, $email, $user->getId());
            }
        
            return $user;
        } catch (\Exception $e) { }

        return null;
    }
    
    /**
     * @param User $user
     * @throws \Exception
     * @return User|null
     */
    public static function create(User $user)
    {
        self::initialize();

        try {
            $response = self::$http->post('users', [
                'form_params' => [
                    'name' => $user->getName(),
                    'email' => $user->getEmail(),
                    'password' => $user->getPassword(),
                    'active' => $user->isActive()
                ]
            ]);
        
            $user = self::manageResponse($response);
            if (!is_null($user)) {
                self::setCache($user, $user->getEmail(), $user->getId());
            }
        
            return $user;
        } catch (\Exception $e) { }
    
        return null;
    }
    
    /**
     * @param User $user
     * @throws \Exception
     * @return User|false
     */
    public static function update(User $user)
    {
        self::initialize();
    
        try {
            $response = self::$http->put('users', [
                'form_params' => [
                    'name' => $user->getName(),
                    'email' => $user->getEmail(),
                    'password' => $user->getPassword(),
                    'active' => $user->isActive()
                ]
            ]);
        
            $user = self::manageResponse($response);
            if (!is_null($user)) {
                self::setCache($user, $user->getEmail(), $user->getId());
            }
            
            return is_null($user) ? false : $user;
        } catch (\Exception $e) { }
    
        return false;
    }

    /**
     * @param string $token
     */
    public static function setApiToken($token)
    {
        self::$api_token = $token;
    }
    
    /**
     * @param ResponseInterface $response
     * @return User|null
     * @throws JsonException
     */
    private static function manageResponse(ResponseInterface $response)
    {
        if ($response->getStatusCode() == 200) {
            $user = json_decode($response->getBody()->getContents());
            if (json_last_error() != JSON_ERROR_NONE) {
                throw new JsonException(
                    $response->getBody()->getContents(),
                    json_last_error_msg(),
                    json_last_error()
                );
            }
        
            /* @var $user \stdClass */
            return (new User((array) $user));
        }
        
        return null;
    }
    
    /**
     * @param null $email
     * @param null $user_id
     * @return User|null
     */
    private static function getCache($email = null, $user_id = null)
    {
        $key = $email;
        if (!is_null($user_id)) {
            $key = $user_id;
        }
    
        return CacheManager::get($key);
    }
    
    /**
     * @param User $user
     * @param null $email
     * @param null $user_id
     */
    private static function setCache(User $user, $email = null, $user_id = null)
    {
        if (!is_null($email)) {
            CacheManager::set($email, $user);
        }
        
        if (!is_null($user_id)) {
            CacheManager::set($user_id, $user);
        }
    }
}
