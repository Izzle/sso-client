<?php
namespace Izzle\SSO\Client\Entities;

class User
{
    /**
     * @var int
     */
    protected $id = 0;
    
    /**
     * @var string
     */
    protected $name = '';
    
    /**
     * @var string
     */
    protected $email = '';
    
    /**
     * @var string
     */
    protected $password = '';
    
    /**
     * @var bool
     */
    protected $active = true;
    
    /**
     * User constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        foreach ($attributes as $attribute => $value) {
            if (property_exists($this, $attribute)) {
                $this->{$attribute} = $value;
            }
        }
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        if (!is_string($name)) {
            throw new \InvalidArgumentException('Param name must be from type string');
        }
        
        $this->name = $name;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        if (!is_string($email)) {
            throw new \InvalidArgumentException('Param email must be from type string');
        }
        
        $this->email = $email;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        if (!is_string($password)) {
            throw new \InvalidArgumentException('Param password must be from type string');
        }
        
        $this->password = $password;
        return $this;
    }
    
    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }
    
    /**
     * @param boolean $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = (bool) $active;
        return $this;
    }
}
