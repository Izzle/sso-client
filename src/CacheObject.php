<?php
namespace Izzle\SSO\Client;

class CacheObject
{
    /**
     * @var \DateTime
     */
    protected $created;
    
    /**
     * @var mixed
     */
    protected $value;
    
    /**
     * CacheObject constructor.
     */
    public function __construct()
    {
        $this->created = new \DateTime();
    }
    
    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
    
    /**
     * @param \DateTime $created
     * @return CacheObject
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * @param mixed $value
     * @return CacheObject
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
