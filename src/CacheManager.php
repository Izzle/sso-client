<?php
namespace Izzle\SSO\Client;

final class CacheManager
{
    /**
     * @var CacheObject[]
     */
    private static $data = [];
    
    /**
     * @var int
     */
    private static $lifetime = 86400; // Seconds
    
    private function __construct() {}
    private function __clone() {}
    
    /**
     * @param int $lifetime
     */
    public static function setLifetime($lifetime)
    {
        if (!is_int($lifetime)) {
            throw new \InvalidArgumentException('Param lifetime must be from type int');
        }
        
        self::$lifetime = $lifetime;
    }
    
    /**
     * @param int|string $key
     * @param mixed $value
     */
    public static function set($key, $value)
    {
        self::checkKey($key);
        
        if (!isset(self::$data[$key])) {
            self::$data[$key] = new CacheObject();
        }
        
        self::$data[$key]->setValue($value);
    }
    
    /**
     * @param int|string $key
     * @return mixed|null
     */
    public static function get($key)
    {
        self::checkKey($key);
        
        if (isset(self::$data[$key])) {
            // Lifetime exceeded
            if (self::$data[$key]->getCreated()->add(new \DateInterval('PT' . self::$lifetime . 'S')) < (new \DateTime())) {
                unset(self::$data[$key]);
                
                return null;
            }
            
            return self::$data[$key]->getValue();
        }
    
        return null;
    }
    
    /**
     * @param int|string $key
     */
    public static function remove($key)
    {
        self::checkKey($key);
        
        if (isset(self::$data[$key])) {
            unset(self::$data[$key]);
        }
    }
    
    /**
     * @param int|string $key
     * @throws \InvalidArgumentException
     */
    private static function checkKey($key)
    {
        if (!is_int($key) && !is_string($key)) {
            throw new \InvalidArgumentException('Param key must be from type int or string');
        }
    }
}
