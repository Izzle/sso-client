<?php
namespace Izzle\SSO\Client\Exception;

use Exception;

class JsonException extends Exception
{
    /**
     * @var string
     */
    protected $content = '';
    
    /**
     * JsonException constructor.
     * @param string $content
     * @param int $message
     * @param Exception $code
     * @param Exception $previous
     */
    public function __construct($content, $message, $code, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        
        $this->content = $content;
    }
    
    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}
