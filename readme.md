# Izzle Single Sign On Client

## Example

```php
use Izzle\SSO\Client\Client;
use Izzle\SSO\Client\Client\Entities\User;

Client::setApiToken('MY_API_TOKEN');
Client::setBaseUrl('SSO_MANAGER_URL');

// Create new user
$user = Client::create((new User([
    'name' => 'Daniel',
    'email' => 'some@email.xx',
    'password' => 'mypassword',
    'active' => true,
])));

// Update user
Client::update((new User([
   'name' => 'Daniel',
   'email' => 'some@email.xx',
   'password' => 'mynewpassword',
   'active' => true,
])));

// Check login
if (Client::check('daniel@somehost.de', 'mynewpassword')) {
    echo 'yeah, credentials are valid';
}
```

## License

Izzle single sign on client is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).